{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Analysing single-cell RNA-sequencing Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial is based on the vignette created by Johannes Griss (2021-10-26\n",
    ") https://www.bioconductor.org/packages/release/bioc/vignettes/ReactomeGSA/inst/doc/analysing-scRNAseq.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "The ReactomeGSA package is a client to the web-based Reactome Analysis System. Essentially, it performs a gene set analysis using the latest version of the Reactome pathway database as a backend.\n",
    "\n",
    "This vignette shows how the ReactomeGSA package can be used to perform a pathway analysis of cell clusters in single-cell RNA-sequencing data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Citation\n",
    "To cite this package, use\n",
    "\n",
    "Griss J. ReactomeGSA, https://github.com/reactome/ReactomeGSA (2019)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Installation\n",
    "The `ReactomeGSA` package can be directly installed from Bioconductor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if (!requireNamespace(\"BiocManager\", quietly = TRUE))\n",
    "    install.packages(\"BiocManager\")\n",
    "\n",
    "if (!require(ReactomeGSA))\n",
    "  BiocManager::install(\"ReactomeGSA\")\n",
    "#> Loading required package: ReactomeGSA\n",
    "\n",
    "# install the ReactomeGSA.data package for the example data\n",
    "if (!require(ReactomeGSA.data))\n",
    "  BiocManager::install(\"ReactomeGSA.data\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more information, see https://bioconductor.org/install/."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Example data\n",
    "As an example we load single-cell RNA-sequencing data of B cells extracted from the dataset published by Jerby-Arnon *et al.* (Cell, 2018).\n",
    "\n",
    "Note: This is not a complete Seurat object. To decrease the size, the object only contains gene expression values and cluster annotations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "library(ReactomeGSA.data)\n",
    "data(jerby_b_cells)\n",
    "\n",
    "jerby_b_cells\n",
    "#> An object of class Seurat \n",
    "#> 23686 features across 920 samples within 1 assay \n",
    "#> Active assay: RNA (23686 features, 0 variable features)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Extracting counts table and metadata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metadata <- jerby_b_cells@meta.data\n",
    "head(metadata)\n",
    "saveRDS(object = metadata, file = file.path( \".\", \"Single-Cell\", \"jerby_b_cells_metadata.RDS\"))\n",
    "write.csv(metadata, row.names = TRUE, file = file.path( \".\", \"Single-Cell\", \"jerby_b_cells_metadata.csv\"), quote = FALSE)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "counts <- jerby_b_cells@assays$RNA@counts\n",
    "head(counts[1:3, 1:5])\n",
    "saveRDS(object = counts, file = file.path( \".\", \"Single-Cell\", \"jerby_b_cells_counts.RDS\"))\n",
    "write.csv(counts, row.names = TRUE, file = file.path( \".\", \"Single-Cell\", \"jerby_b_cells_counts.csv\"), quote = FALSE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How to create Seurat Object from data in text comma separated values (csv) format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "counts <- read.csv(  file = file.path( \".\", \"Single-Cell\", \"jerby_b_cells_counts.csv\"))\n",
    "# Set first column as row names an dremove it \n",
    "rownames(counts) <- counts[,\"X\"]\n",
    "counts <- counts[,setdiff(colnames(counts), \"X\")]\n",
    "\n",
    "\n",
    "metadata <- read.csv( file = file.path( \".\", \"Single-Cell\", \"jerby_b_cells_metadata.csv\"))\n",
    "rownames(metadata) <- metadata[,\"X\"]\n",
    "metadata <- metadata[,setdiff(colnames(metadata), \"X\")]\n",
    "\n",
    "jerby_b_cells <- CreateSeuratObject( counts = counts, \n",
    "                                  meta.data = metadata)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pathway analysis of cell clusters\n",
    "The pathway analysis is at the very end of a scRNA-seq workflow. This means, that any Q/C was already performed, the data was normalized and cells were already clustered.\n",
    "\n",
    "The ReactomeGSA package can now be used to get pathway-level expression values for every cell cluster. This is achieved by calculating the mean gene expression for every cluster and then submitting this data to a gene set variation analysis.\n",
    "\n",
    "All of this is wrapped in the single `analyse_sc_clusters` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "Pl0lFUANx2gZ",
    "outputId": "ac9344c4-c99c-4d2c-ffc3-c54ca3fa1114"
   },
   "outputs": [],
   "source": [
    "## -----------------------------------------------------------------------------\n",
    "library(ReactomeGSA)\n",
    "\n",
    "gsva_result <- analyse_sc_clusters(jerby_b_cells, \n",
    "                                    use_interactors = TRUE, # This is the default\n",
    "                                    include_disease_pathways = TRUE,\n",
    "                                    create_reactome_visualization = TRUE,\n",
    "                                    create_reports = TRUE,\n",
    "                                    report_email = TRUE,\n",
    "                                    verbose = TRUE)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting object is a standard `ReactomeAnalysisResult` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 146
    },
    "id": "3B2gqG8hx5vC",
    "outputId": "b33c6fa1-f481-4f49-9510-2d96ca522844"
   },
   "outputs": [],
   "source": [
    "gsva_result\n",
    "#> ReactomeAnalysisResult object\n",
    "#>   Reactome Release: 77\n",
    "#>   Results:\n",
    "#>   - Seurat:\n",
    "#>     1729 pathways\n",
    "#>     10862 fold changes for genes\n",
    "#>   No Reactome visualizations available\n",
    "#> ReactomeAnalysisResult"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "`pathways` returns the pathway-level expression values per cell cluster:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 191
    },
    "id": "pltWpFBMx7m5",
    "outputId": "0feb5eb6-34a2-4a28-b3ba-c7e85ae818bc"
   },
   "outputs": [],
   "source": [
    "pathway_expression <- pathways(gsva_result)\n",
    "\n",
    "# simplify the column names by removing the default dataset identifier\n",
    "colnames(pathway_expression) <- gsub(\"\\\\.Seurat\", \"\", colnames(pathway_expression))\n",
    "\n",
    "pathway_expression[1:3,]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A simple approach to find the most relevant pathways is to assess the maximum difference in expression for every pathway:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 284
    },
    "id": "Sa_-NN95x_VJ",
    "outputId": "0824794a-7ebf-4d44-96eb-514bed71c6f2"
   },
   "outputs": [],
   "source": [
    "# find the maximum differently expressed pathway\n",
    "max_difference <- do.call(rbind, apply(pathway_expression, 1, function(row) {\n",
    "    values <- as.numeric(row[2:length(row)])\n",
    "    return(data.frame(name = row[1], min = min(values), max = max(values)))\n",
    "}))\n",
    "\n",
    "max_difference$diff <- max_difference$max - max_difference$min\n",
    "\n",
    "# sort based on the difference\n",
    "max_difference <- max_difference[order(max_difference$diff, decreasing = T), ]\n",
    "\n",
    "head(max_difference)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Plotting the results\n",
    "The ReactomeGSA package contains two functions to visualize these pathway results. The first simply plots the expression for a selected pathway:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a better overview, the expression of multiple pathways can be shown as a heatmap using `gplots` `heatmap.2` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 437
    },
    "id": "kg9sOdCL-Qie",
    "outputId": "5653fa2d-eb5a-46b3-ae75-a7bf2ea3056b"
   },
   "outputs": [],
   "source": [
    "plot_gsva_pathway(gsva_result, pathway_id = rownames(max_difference)[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plot_gsva_heatmap function can also be used to only display specific pathways:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 437
    },
    "id": "V8PuBXlv-cIm",
    "outputId": "8ef0504c-e2f8-417a-bbc6-c3e1d1d8d153"
   },
   "outputs": [],
   "source": [
    "# limit to selected B cell related pathways\n",
    "relevant_pathways <- c(\"R-HSA-983170\", \"R-HSA-388841\", \"R-HSA-2132295\", \"R-HSA-983705\", \"R-HSA-5690714\")\n",
    "plot_gsva_heatmap(gsva_result, \n",
    "                  pathway_ids = relevant_pathways, # limit to these pathways\n",
    "                  margins = c(6,30), # adapt the figure margins in heatmap.2\n",
    "                  dendrogram = \"col\", # only plot column dendrogram\n",
    "                  scale = \"row\", # scale for each pathway\n",
    "                  key = FALSE, # don't display the color key\n",
    "                  lwid=c(0.1,4)) # remove the white space on the left\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This analysis shows us that cluster 8 has a marked up-regulation of B Cell receptor signalling, which is linked to a co-stimulation of the CD28 family. Additionally, there is a gradient among the cluster with respect to genes releated to antigen presentation.\n",
    "\n",
    "Therefore, we are able to further classify the observed B cell subtypes based on their pathway activity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pathway-level PCA\n",
    "\n",
    "The pathway-level expression analysis can also be used to run a Principal Component Analysis on the samples. This is simplified through the function `plot_gsva_pca`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 437
    },
    "id": "766oWD5e-fZj",
    "outputId": "69263924-fcb8-4d91-d3f0-0594dc14f292"
   },
   "outputs": [],
   "source": [
    "plot_gsva_pca(gsva_result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this analysis, cluster 11 is a clear outlier from the other B cell subtypes and therefore might be prioritised for further evaluation.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reactome Pathway Visualization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the URL link to visualize the Reactome Pathways analysis results\n",
    "reactome_links(gsva_result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the URL link to visualize the Reactome Pathways analysis results\n",
    "open_reactome(gsva_result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional Information\n",
    "The following hyperlink provides a tutorial on how to perform single-cell analysis with Seurat, preform normalization and clustering on the data, prior to the Reactome pathways analysis. Please visit if you're interested. https://github.com/reactome/ReactomeGSA-tutorials/blob/master/notebooks/Jerby_Arnon_Seurat.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Session Info"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 1000
    },
    "id": "rjUSxNRz-jfC",
    "outputId": "4dbe22e4-3b1d-4720-ca6f-9b4c2ae0b0aa"
   },
   "outputs": [],
   "source": [
    "sessionInfo()"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "collapsed_sections": [],
   "name": "Reactome GSA single cell.ipynb",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "R 4.0.3",
   "language": "R",
   "name": "ir403"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "4.0.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
